#!/usr/bin/env python
import os
from dotenv import load_dotenv
from dotenv import dotenv_values

from pathlib import Path
import pymailer

cwd = os.getcwd()

dotenv_path = Path(cwd + '/.env')
load_dotenv(dotenv_path)


SUBJECT=os.getenv('SUBJECT')
BODY=os.getenv('BODY')
SENDER=os.getenv('SENDER')
RECIPIENT=os.getenv('RECIPIENT') # .split(',') if working with list/array
CREDENTIALS=os.getenv('CREDENTIALS')
DEBUG_LEVEL=os.getenv('DEBUG_LEVEL')
print(dotenv_path)
print(SUBJECT, 
        BODY, 
        SENDER, 
        RECIPIENT, 
        CREDENTIALS, 
        DEBUG_LEVEL)
pymailer.pymailer(SUBJECT, 
        BODY, 
        SENDER, 
        RECIPIENT, 
        CREDENTIALS, 
        DEBUG_LEVEL)
#print(obj)
