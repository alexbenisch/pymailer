#!/usr/bin/env python
import json
import smtplib
from email.mime.text import MIMEText
def pymailer(subject, body, sender, recipient, credentials, debuglevel):
    debuglevel=int(debuglevel)
    credentials_json = json.loads(credentials)
    subject = subject
    body = body
    message = f"{subject}\n\n{body}"
    msg = MIMEText(message)
    username = credentials_json['username']
    password = credentials_json['password']
    msg['From'] = sender
    recipient = recipient
    #msg['To'] = ", ".join(recipients) #If working with an array/list
    msg['To'] = recipient
    msg['Subject'] = subject
    print(recipient)
    with smtplib.SMTP(credentials_json['server'], 
    credentials_json['port']) as smtp:
        smtp.set_debuglevel(debuglevel)
        smtp.ehlo()
        smtp.starttls() 
        smtp.ehlo()

        smtp.login(username, password)

        smtp.sendmail(msg['From'], recipient, msg.as_string())

